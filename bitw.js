require("dotenv").config();
const puppeteer = require('puppeteer');


let pw = process.env.BW_PASSW
let usr = process.env.BW_EMAIL;

(async () => {

    const browser = await puppeteer.launch({headless: false,
    args:[
        '--start-maximized'
    ]});

    const page = await browser.newPage();
    page.setViewport({
        width: 1920,
        height: 1080,
        isMobile: false
    });

    //navigate to Bitwarden site
    const response = await page.goto('https://vault.bitwarden.com/#/lock')

    //enter creds
    await page.waitForSelector('#email');
    await page.type('#email', usr)
    await page.type('#masterPassword', pw)

    //login
    await page.click('body > app-root > app-frontend-layout > app-login > form > div > div > div > div > div:nth-child(6) > button')
    await page.waitForSelector('body > app-root > app-user-layout > app-vault > div > div > div.col-6 > div > h1')

    //navigate to tools
    await page.click('body > app-root > app-user-layout > app-navbar > nav > div > div > ul > li:nth-child(3) > a')
    await page.waitForSelector('body > app-root > app-user-layout > app-tools > div > div > div.col-3 > div.card.mb-4 > div.list-group.list-group-flush > a:nth-child(3)')

    //navigate to export vault
    await page.click('body > app-root > app-user-layout > app-tools > div > div > div.col-3 > div.card.mb-4 > div.list-group.list-group-flush > a:nth-child(3)')
    //export
    await page.waitForSelector('body > app-root > app-user-layout > app-tools > div > div > div.col-9 > app-export > form > button')
    await page.type('#masterPassword', pw)
    await page.click('body > app-root > app-user-layout > app-tools > div > div > div.col-9 > app-export > form > button')

    //confirm export
    await page.waitForSelector('#swal2-title')
    await page.click('body > div > div > div.swal2-actions > button.swal2-confirm')

    await page.waitForSelector('body > app-root > toaster-container > div > div > div')
    console.log(`verified export success`);

    await delay(4000)

    await browser.close();

})();